"""
Ce script permet d'effectuer automatiquement des combats dans FF9 avec les inputs clavier.
"""

from __future__ import print_function

import time
from inputs import devices
import pydirectinput
from PIL import ImageGrab

def main():
    black=(0,0,0)
    fightBegin=False
    fightEnd=False
    time.sleep(5)
    pydirectinput.keyDown('left')
    while 1:
        pydirectinput.keyDown('left')
        pydirectinput.press('x', presses=5, interval=0.1)
        pydirectinput.keyUp('left')

if __name__ == "__main__":
    main()
