"""
Ce script permet d'effectuer automatiquement des combats dans FF8 avec les inputs clavier.
"""

from __future__ import print_function

import time
from inputs import devices
import pydirectinput
from PIL import ImageGrab

def main():
    print("We have detected the following devices:\n")
    for device in devices:
         print(device)
    
    black=(0,0,0)
    fightBegin=False
    fightEnd=False
    time.sleep(5)
    pydirectinput.keyDown('left')
    while 1:
        px = ImageGrab.grab().load()
        pxColorList=[]
        for y in range(500, 600, 10):
            for x in range(500, 600, 10):
                color = px[x, y]
                pxColorList.append(color)
        time.sleep(0.2)
        if pxColorList.count(black) > 30 and not fightBegin: 
            print("FIGHT BEGIN")
            fightBegin=True
            pydirectinput.keyUp('left')
            pydirectinput.keyDown('x')
            time.sleep(7)

        elif pxColorList.count(black) > 30 and fightBegin:
            print("This is the end")
            time.sleep(1)
            fightBegin=False
            pydirectinput.keyUp('x')
            pydirectinput.keyDown('left')
            pydirectinput.press('x', presses=10, interval=0.25)
             
if __name__ == "__main__":
    main()
